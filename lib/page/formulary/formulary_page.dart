import 'package:app_camiones/services/services.dart';
import 'package:app_camiones/widget/widget.dart';
import 'package:flutter/material.dart';

class FormularyPage extends StatefulWidget {
  FormularyPage({super.key});

  @override
  State<FormularyPage> createState() => _FormularyPageState();
}

class _FormularyPageState extends State<FormularyPage> {
  TextEditingController marca = TextEditingController();
  TextEditingController color = TextEditingController();
  TextEditingController anio = TextEditingController();
  TextEditingController kilometraje = TextEditingController();
  TextEditingController TipoCombustible = TextEditingController();

  GlobalKey<FormState> keyForm = GlobalKey();
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldkey,
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text('Formulario camiones'),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: [
              Form(
                key: keyForm,
                child: Column(
                  children: [
                    formText('Marca', marca),
                    formText('Color', color),
                    formText('Año', anio, textInputType: TextInputType.number),
                    formText('Kilometraje', kilometraje,
                        textInputType: TextInputType.number),
                    formText('Tipo de Combustible', TipoCombustible),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String? response = await DioServices().EnviarDatos(
                            marca.text,
                            color.text,
                            anio.text,
                            kilometraje.text,
                            TipoCombustible.text);
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(response ?? ''), duration: Duration(seconds: 10),));
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.blueAccent,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        padding: EdgeInsets.all(15),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              'Guardar',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ))
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
