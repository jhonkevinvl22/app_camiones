import 'dart:developer';

import 'package:dio/dio.dart';

class DioServices {
  Duration connectTimeout = Duration(minutes: 1);
  Duration receiveTimeout = Duration(minutes: 1);
  Dio dio = Dio();
  String urlPrincipal = 'http://example.com?';

  Future<String?> EnviarDatos(marca, color, anio, km, TCombustible) async {
    dio.options.connectTimeout = connectTimeout;
    dio.options.receiveTimeout = receiveTimeout;
    try {
      String? url =
          '${urlPrincipal}${marca != '' ? 'marca=${marca}' : ''}${marca != '' ? '&' : ''}${color != '' ? 'color=${color}' : ''}${color != '' ? '&' : ''}${anio != '' ? 'anio=${anio}' : ''}${anio != '' ? '&' : ''}${km != '' ? 'kilometraje=${km}' : ''}${km != '' ? '&' : ''}${TCombustible != '' ? 'TipoCombustible=${TCombustible}' : ''}';
      log(url);
      return url;
      // final response = await dio.get(url);
      // if (response.statusCode == 200) {
      //   return true;
      // } else {
      //   return false;
      // }
    } on DioError catch (e) {
      log(e.toString());
      return '';
    }
  }
}
