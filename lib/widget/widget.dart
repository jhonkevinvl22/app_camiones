import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget formText(String title, TextEditingController marca,
    {TextInputType? textInputType = TextInputType.text}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
        controller: marca,
        keyboardType: textInputType,
      ),
      SizedBox(height: 20,)
    ],
  );
}
